/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Header_Request.hpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fassani <fassani@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/01 10:45:22 by fassani           #+#    #+#             */
/*   Updated: 2021/06/08 16:23:15 by fassani          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HEADER_REQUEST_HPP
#define HEADER_REQUEST_HPP

#include <ostream>
#include <map>
#include "Header_Structures.hpp"
#include "Header_Parsing.hpp"
#include "../utils/utils.hpp"

class Header_request
{
public:
    Header_request();
    ~Header_request();
    Header_request(const Header_request &other);
    const Header_request &operator=(const Header_request &other);
    Header_request(std::string str);
    std::string Header_toString();

public:

    std::map<std::string, std::string> &getMap_header();
    size_t  getHeader_length();
    std::string getBody_tmp_path();
    t_Request getRequest();
    std::vector<t_Charset> getAccept_Charset();
    std::vector<t_Language> getAccept_Language();
    t_Allow getAllow();
    t_Authorization getAuthorization();
    std::vector<std::string> getContent_Language();
    size_t getContent_Length();
    std::string getContent_Location();
    t_Type getContent_Type();
    t_Date getDate();
    t_Host getHost();
    t_Date getLast_Modified();
    std::string getLocation();
    std::string getReferer();
    t_Retry_After getRetry_After();
    std::string getServer();
    std::vector<std::string> getTransfer_Encoding();
    std::string getUser_Agent();
    t_WWW_Authenticate getWWW_Authenticate();

    void setHeader_length(size_t length);
    void setBody_tmp_path(std::string path);
    void setRequest(std::string method, std::string path, std::string query, std::string path_info, std::string version);
    void setAccept_Charset(std::vector<t_Charset> accept_charset);
    void setAccept_Language(std::vector<t_Language> accept_language);
    void setAllow(t_Allow allow);
    void setAuthorization(std::string type, std::string credentials);
    void setContent_Language(std::vector<std::string> languages);
    void setContent_Length(size_t length);
    void setContent_Location(std::string location);
    void setContent_Type(std::string media_type, std::string charset, std::string boundary);
    void setDate(t_Date date);
    void setHost(std::string host, std::string port);
    void setLast_Modified(t_Date last_modified);
    void setLocation(std::string location);
    void setReferer(std::string referer);
    void setRetry_After(t_Date http_date, size_t delay_seconds);
    void setServer(std::string server);
    void setTransfer_Encoding(std::vector<std::string> transfer_encoding);
    void setUser_Agent(std::string user_agent);
    void setWWW_Authenticate(std::string type, std::string realm, std::string charset);

    std::string Accept_Charset_toString();
    std::string Accept_Language_toString();
    std::string Allow_toString();
    std::string Authorization_toString();
    std::string Content_Language_toString();
    std::string Content_Length_toString();
    std::string Content_Location_toString();
    std::string Content_Type_toString();
    std::string Date_toString();
    std::string Host_toString();
    std::string Last_Modified_toString();
    std::string Location_toString();
    std::string Referer_toString();
    std::string Retry_After_toString();
    std::string Server_toString();
    std::string Transfer_Encoding_toString();
    std::string User_Agent_toString();
    std::string WWW_Authenticate_toString();

private:
    std::map<std::string, std::string> map_header;

    size_t header_length;

    std::string body_tmp_path;

    //https://developer.mozilla.org/fr/docs/Web/HTTP/Overview
    t_Request Request;

    //https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/Accept-Charset
    std::vector<t_Charset> Accept_Charset;

    // https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/Accept-Language
    std::vector<t_Language> Accept_Language;

    // https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/Allow
    t_Allow Allow;

    // https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/Authorization
    t_Authorization Authorization;

    // https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/Content-Language
    std::vector<std::string> Content_Language;

    //https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/Content-Length
    size_t Content_Length;

    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Location
    std::string Content_Location;

    // https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/Content-Type
    t_Type Content_Type;

    // https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/Date
    t_Date Date;

    // https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/Host
    t_Host Host;

    // https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/Last-Modified
    t_Date Last_Modified;

    // https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/Location
    std::string Location;

    // https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/Referer
    std::string Referer;

    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Retry-After
    t_Retry_After Retry_After;

    // https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/Server
    std::string Server;

    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Transfer-Encoding
    std::vector<std::string> Transfer_Encoding;

    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/User-Agent
    std::string User_Agent;

    // https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/WWW-Authenticate
    t_WWW_Authenticate WWW_Authenticate;
};

std::ostream &operator<<(std::ostream &out, const Header_request &header);

#endif