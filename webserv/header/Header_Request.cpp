/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Header_Request.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fassani <fassani@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/01 12:22:33 by fassani           #+#    #+#             */
/*   Updated: 2021/06/08 18:17:58 by fassani          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../Webserv.hpp"
#include "Header_Request.hpp"
#define COUT std::cout
#define ENDL std::endl

Header_request::Header_request()
{
}

Header_request::~Header_request()
{
    map_header.clear();
}

Header_request::Header_request(const Header_request &other)
{
    *this = other;
}

const Header_request &Header_request::operator=(const Header_request &other)
{
    this->map_header = other.map_header;
    this->header_length = other.header_length;
    this->body_tmp_path = other.body_tmp_path;
    this->Request = other.Request;
    this->Accept_Charset = other.Accept_Charset;
    this->Accept_Language = other.Accept_Language;
    this->Allow = other.Allow;
    this->Authorization = other.Authorization;
    this->Content_Language = other.Content_Language;
    this->Content_Length = other.Content_Length;
    this->Content_Location = other.Content_Location;
    this->Content_Type = other.Content_Type;
    this->Date = other.Date;
    this->Host = other.Host;
    this->Last_Modified = other.Last_Modified;
    this->Location = other.Location;
    this->Referer = other.Referer;
    this->Retry_After = other.Retry_After;
    this->Server = other.Server;
    this->Transfer_Encoding = other.Transfer_Encoding;
    this->User_Agent = other.User_Agent;
    this->WWW_Authenticate = other.WWW_Authenticate;

    return (*this);
}

std::string Header_request::Header_toString()
{
    std::string str = NULL;

    str.append(this->Accept_Charset_toString());
    str.append(this->Accept_Language_toString());
    str.append(this->Allow_toString());
    str.append(this->Authorization_toString());
    str.append(this->Content_Language_toString());
    str.append(this->Content_Length_toString());
    str.append(this->Content_Location_toString());
    str.append(this->Content_Type_toString());
    str.append(this->Date_toString());
    str.append(this->Host_toString());
    str.append(this->Last_Modified_toString());
    str.append(this->Location_toString());
    str.append(this->Referer_toString());
    str.append(this->Retry_After_toString());
    str.append(this->Server_toString());
    str.append(this->Transfer_Encoding_toString());
    str.append(this->User_Agent_toString());
    str.append(this->WWW_Authenticate_toString());

    return (str);
}

Header_request::Header_request(std::string str)
{

    //super parsing du header qui ne sert à rien
    //manque le parsing de Retry_After
    if (str.empty())
        return;

    this->header_length = (sizeof(str[0]) * str.size()) + 4;

    // COUT << YELLOW << str << RESET << ENDL;

    std::vector<std::string> list_header = split_string(str, "\n");

    if (list_header.size() == 0)
    {
        // COUT << "erreur dans le header" << ENDL;
        return;
    }

    this->Request = parsing_header_request(list_header.at(0));
    list_header.erase(list_header.begin());

    if (list_header.size() == 0)
    {
        // COUT << "erreur dans le header" << ENDL;
        return;
    }

    this->map_header.clear();

    for (size_t i = 0; i < list_header.size(); i++)
    {
        std::string key;
        int position = 0;
        position = list_header.at(i).find(':', 0);
        key = list_header.at(i).substr(0, position);
        position++;
        this->map_header[key] = list_header.at(i).substr(position);
        if (this->map_header[key].at(0) == ' ')
            this->map_header[key].erase(0, 1);
    }

    this->Accept_Charset = parsing_Accept_Charset(this->map_header["Accept-Charset"]);
    this->Accept_Language = parsing_Accept_Language(this->map_header["Accept-Language"]);
    this->Allow = parsing_Allow(this->map_header["Allow"]);
    this->Authorization = parsing_Authorization(this->map_header["Authorization"]);
    this->Content_Language = parsing_Content_Language(this->map_header["Content-Language"]);
    this->Content_Length = parsing_Content_Length(this->map_header["Content-Length"]);
    this->Content_Location = parsing_Content_Location(this->map_header["Content-Location"]);
    this->Content_Type = parsing_Content_Type(this->map_header["Content-Type"]);
    this->Date = parsing_Date(this->map_header["Date"]);
    this->Host = parsing_Host(this->map_header["Host"]);
    this->Last_Modified = parsing_Last_Modified(this->map_header["Last-Modified"]);
    this->Location = parsing_Location(this->map_header["Location"]);
    this->Referer = parsing_Referer(this->map_header["Referer"]);
    this->Retry_After = parsing_Retry_After(this->map_header["Retry-After"]);
    this->Server = parsing_Server(this->map_header["Server"]);
    this->Transfer_Encoding = parsing_Transfer_Encoding(this->map_header["Transfer-Encoding"]);
    this->User_Agent = parsing_User_Agent(this->map_header["User-Agent"]);
    this->WWW_Authenticate = parsing_WWW_Authenticate(this->map_header["WWW-Authenticate"]);
    return;
}

std::ostream &operator<<(std::ostream &out, Header_request &header)
{
    return (out << header.Header_toString());
}

//////////////////////GET

std::map<std::string, std::string> &Header_request::getMap_header()
{
    return (this->map_header);
}

size_t Header_request::getHeader_length()
{
    return (this->header_length);
}

std::string Header_request::getBody_tmp_path()
{
    return (this->body_tmp_path);
}

t_Request Header_request::getRequest()
{
    return (this->Request);
}
std::vector<t_Charset> Header_request::getAccept_Charset()
{
    return (this->Accept_Charset);
}

std::vector<t_Language> Header_request::getAccept_Language()
{
    return (this->Accept_Language);
}

t_Allow Header_request::getAllow()
{
    return (this->Allow);
}

t_Authorization Header_request::getAuthorization()
{
    return (this->Authorization);
}

std::vector<std::string> Header_request::getContent_Language()
{
    return (this->Content_Language);
}

size_t Header_request::getContent_Length()
{
    return (this->Content_Length);
}

std::string Header_request::getContent_Location()
{
    return (this->Content_Location);
}

t_Type Header_request::getContent_Type()
{
    return (this->Content_Type);
}

t_Date Header_request::getDate()
{
    return (this->Date);
}

t_Host Header_request::getHost()
{
    return (this->Host);
}

t_Date Header_request::getLast_Modified()
{
    return (this->Last_Modified);
}

std::string Header_request::getLocation()
{
    return (this->Location);
}

std::string Header_request::getReferer()
{
    return (this->Referer);
}

t_Retry_After Header_request::getRetry_After()
{
    return (this->Retry_After);
}

std::string Header_request::getServer()
{
    return (this->Server);
}

std::vector<std::string> Header_request::getTransfer_Encoding()
{
    return (this->Transfer_Encoding);
}

std::string Header_request::getUser_Agent()
{
    return (this->User_Agent);
}

t_WWW_Authenticate Header_request::getWWW_Authenticate()
{
    return (this->WWW_Authenticate);
}

//////////////////////SET

void Header_request::setHeader_length(size_t length)
{
    this->header_length = length;
}

void Header_request::setBody_tmp_path(std::string path)
{
    this->body_tmp_path = path;
}

void Header_request::setRequest(std::string method, std::string path, std::string query, std::string path_info, std::string version)
{
    this->Request.Method = method;
    this->Request.Path = path;
    this->Request.Path_Info = path_info;
    this->Request.Query = query;
    this->Request.Version = version;
}

void Header_request::setAccept_Charset(std::vector<t_Charset> accept_charset)
{
    this->Accept_Charset = accept_charset;
}

void Header_request::setAccept_Language(std::vector<t_Language> accept_language)
{
    this->Accept_Language = accept_language;
}

void Header_request::setAllow(t_Allow allow)
{
    this->Allow = allow;
}

void Header_request::setAuthorization(std::string type, std::string credentials)
{
    this->Authorization.type = type;
    this->Authorization.credentials = credentials;
}

void Header_request::setContent_Language(std::vector<std::string> languages)
{
    this->Content_Language = languages;
}

void Header_request::setContent_Length(size_t length)
{
    this->Content_Length = length;
}

void Header_request::setContent_Location(std::string location)
{
    this->Content_Location = location;
}

void Header_request::setContent_Type(std::string media_type, std::string charset, std::string boundary)
{
    this->Content_Type.media_type = media_type;
    this->Content_Type.charset = charset;
    this->Content_Type.boundary = boundary;
}

void Header_request::setDate(t_Date date)
{
    this->Date = date;
}

void Header_request::setHost(std::string host, std::string port)
{
    this->Host.host = host;
    this->Host.port = port;
}

void Header_request::setLast_Modified(t_Date last_modified)
{
    this->Last_Modified = last_modified;
}

void Header_request::setLocation(std::string location)
{
    this->Location = location;
}

void Header_request::setReferer(std::string referer)
{
    this->Referer = referer;
}

void Header_request::setRetry_After(t_Date http_date, size_t delay_seconds)
{
    this->Retry_After.http_date = http_date;
    this->Retry_After.delay_seconds = delay_seconds;
}

void Header_request::setServer(std::string server)
{
    this->Server = server;
}

void Header_request::setTransfer_Encoding(std::vector<std::string> transfer_encoding)
{
    this->Transfer_Encoding = transfer_encoding;
}

void Header_request::setUser_Agent(std::string user_agent)
{
    this->User_Agent = user_agent;
}

void Header_request::setWWW_Authenticate(std::string type, std::string realm, std::string charset)
{
    this->WWW_Authenticate.type = type;
    this->WWW_Authenticate.realm = realm;
    this->WWW_Authenticate.charset = charset;
}

//////////////////////TO_STRING

// syntax: Accept-Charset: <charset>
// exemple: Accept-Charset: utf-8, iso-8859-1;q=0.5, *;q=0.1
std::string Header_request::Accept_Charset_toString()
{
    if (this->Accept_Charset.empty())
        return ("");

    std::string str;
    for (size_t i = 0; i < this->Accept_Charset.size(); i++)
    {
        if (!this->Accept_Charset.at(i).charset.empty())
        {
            str.append(" " + Accept_Charset.at(i).charset);
            if (this->Accept_Charset.at(i).q != 0)
                str.append(";" + to_string(Accept_Charset.at(i).q));
            if (i < this->Accept_Charset.size() - 1)
                str.append(",");
        }
    }
    return ("Accept-Charset: " + str + "\n");
}

// syntax: Accept-Language: <langue>
// syntax: Accept-Language: <locale>
// syntax: Accept-Language: *
// exemple: Accept-Language: fr-CH, fr;q=0.9, en;q=0.8, de;q=0.7, *;q=0.5
std::string Header_request::Accept_Language_toString()
{
    if (this->Accept_Language.empty())
        return ("");

    std::string str;
    for (size_t i = 0; i < this->Accept_Language.size(); i++)
    {
        if (!this->Accept_Language.at(i).langue.empty())
        {
            str.append(this->Accept_Language.at(i).langue);
            if (!this->Accept_Language.at(i).locale.empty())
                str.append("-" + this->Accept_Language.at(i).locale);
            if (this->Accept_Language.at(i).q != 0)
                str.append(";" + to_string(this->Accept_Language.at(i).q));
            if (i < this->Accept_Language.size() - 1)
                str.append(",");
        }
    }
    return ("Accept-Language: " + str + "\n");
}

// syntax: Allow: <methodes-http>
// exemple: Allow: GET, POST, HEAD
std::string Header_request::Allow_toString()
{
    std::string str;

    if (this->Allow.CONNECT == 1)
        str.append("CONNECT, ");
    if (this->Allow.DELETE == 1)
        str.append("DELETE, ");
    if (this->Allow.GET == 1)
        str.append("GET, ");
    if (this->Allow.HEAD == 1)
        str.append("HEAD, ");
    if (this->Allow.OPTIONS == 1)
        str.append("OPTIONS, ");
    if (this->Allow.PATCH == 1)
        str.append("PATCH, ");
    if (this->Allow.POST == 1)
        str.append("POST, ");
    if (this->Allow.PUT == 1)
        str.append("PUT, ");
    if (this->Allow.TRACE == 1)
        str.append("TRACE, ");

    if (str.empty())
        return ("");
    str.resize(str.size() - 2);

    return ("Allow: " + str + "\n");
}

// syntax: Authorization: <type> <credentials>
// exemple: Authorization: Basic YWxhZGRpbjpvcGVuc2VzYW1l
std::string Header_request::Authorization_toString()
{
    if (this->Authorization.type.empty() || !this->Authorization.credentials.empty())
        return ("");

    std::string str;
    str.append(this->Authorization.type + " " + this->Authorization.credentials);

    return ("Authorization: " + str + "\n");
}

// syntax: Content-Language: de-DE
// syntax: Content-Language: en-US
// syntax: Content-Language: de-DE, en-CA
// exemple: Content-Language: de, en
std::string Header_request::Content_Language_toString()
{
    if (this->Content_Language.empty())
        return ("");

    std::string str;

    for (size_t i = 0; i < this->Content_Language.size(); i++)
    {
        str.append(this->Content_Language.at(i));

        if (i < this->Accept_Language.size() - 1)
            str.append(", ");
    }
    return ("Content-Language: " + str + "\n");
}

// syntax: Content-Length: <longueur>
// exemple:  Content-Length: 2088
std::string Header_request::Content_Length_toString()
{
    if (this->Content_Length == 0)
        return ("");

    return ("Content-Length: " + to_string(this->Content_Length) + "\n");
}

// syntaxt: Content-Location: <url>
// exemple: Content-Location: /my-first-blog-post
std::string Header_request::Content_Location_toString()
{
    if (this->Content_Location.empty())
        return ("");

    return ("Content-Location: " + this->Content_Location + "\n");
}

// syntaxt: Content-Type: text/html; charset=utf-8
// syntaxt: Content-Type: multipart/form-data; boundary=something
// exemple: Content-Type: multipart/form-data; boundary=---------------------------974767299852498929531610575
std::string Header_request::Content_Type_toString()
{
    if (this->Content_Type.media_type.empty())
        return ("");
    std::string str;

    str.append(this->Content_Type.media_type + "; ");
    if (!this->Content_Type.charset.empty())
        str.append("charset=" + this->Content_Type.charset + "; ");
    if (!this->Content_Type.boundary.empty())
        str.append("boundary=" + this->Content_Type.boundary + "; ");

    str.resize(str.size() - 2);

    return ("Content-Type: " + str + "\n");
}

// syntax: Date: <day-name>, <jour> <mois> <année> <heure>:<minute>:<seconde> GMT
// exemple: Date: Wed, 21 Oct 2015 07:28:00 GMT
std::string Header_request::Date_toString()
{
    if (this->Date.day_name.empty() || this->Date.day.empty() || this->Date.month.empty() || this->Date.year.empty() || this->Date.hour.empty() || this->Date.minute.empty() || this->Date.second.empty())
        return ("");

    std::string str;

    str.append(this->Date.day_name + ", ");
    str.append(this->Date.day + " ");
    str.append(this->Date.month + " ");
    str.append(this->Date.year + " ");
    str.append(this->Date.hour + ":");
    str.append(this->Date.minute + ":");
    str.append(this->Date.second + " GMT");

    return ("Date: " + str + "\n");
}

// syntax: Host: <host>:<port>
// exemple: Host: developer.mozilla.org
std::string Header_request::Host_toString()
{
    if (this->Host.host.empty())
        return ("");

    std::string str;
    str.append(this->Host.host);
    if (!this->Host.port.empty())
        str.append(":" + this->Host.host);

    return ("Host: " + str + "\n");
}

// syntax: Last-Modified: <nom-jour>, <jour> <mois> <année> <heure>:<minute>:<seconde> GMT
// exemple: Last-Modified: Wed, 21 Oct 2015 07:28:00 GMT
std::string Header_request::Last_Modified_toString()
{
    if (this->Date.day_name.empty() || this->Date.day.empty() || this->Date.month.empty() || this->Date.year.empty() || this->Date.hour.empty() || this->Date.minute.empty() || this->Date.second.empty())
        return ("");

    std::string str;

    str.append(this->Date.day_name + ", ");
    str.append(this->Date.day + " ");
    str.append(this->Date.month + " ");
    str.append(this->Date.year + " ");
    str.append(this->Date.hour + ":");
    str.append(this->Date.minute + ":");
    str.append(this->Date.second + " GMT");

    return ("Last-Modified: " + str + "\n");
}

// syntax: Location: <url>
// exemple: Location: /index.html
std::string Header_request::Location_toString()
{
    if (this->Location.empty())
        return ("");

    return ("Location: " + this->Location + "\n");
}

// syntax: Referer: <url>
// exemple: Referer: https://developer.mozilla.org/fr/docs/Web/JavaScript
std::string Header_request::Referer_toString()
{
    if (this->Referer.empty())
        return ("");

    return ("Referer: " + this->Referer + "\n");
}

// syntax: Retry-After: <http-date>
// syntax: Retry-After: <delay-seconds>
// exemple: Retry-After: Wed, 21 Oct 2015 07:28:00 GMT
// exemple: Retry-After: 120
std::string Header_request::Retry_After_toString()
{
    if (this->Date.day_name.empty() || this->Date.day.empty() || this->Date.month.empty() || this->Date.year.empty() || this->Date.hour.empty() || this->Date.minute.empty() || this->Date.second.empty())
        return ("");

    std::string str;

    str.append(this->Retry_After.http_date.day_name + ", ");
    str.append(this->Retry_After.http_date.day + " ");
    str.append(this->Retry_After.http_date.month + " ");
    str.append(this->Retry_After.http_date.year + " ");
    str.append(this->Retry_After.http_date.hour + ":");
    str.append(this->Retry_After.http_date.minute + ":");
    str.append(this->Retry_After.http_date.second + " GMT\n");

    str.append("Retry-After: " + to_string(this->Retry_After.delay_seconds));

    return ("Retry-After: " + str + "\n");
}

// syntax: Server: <valeur>
// exemple: Server: Apache/2.4.1 (Unix)
std::string Header_request::Server_toString()
{
    if (this->Server.empty())
        return ("");
    return ("Server: " + this->Server + "\n");
}

// syntax: Transfer-Encoding: <value>
// exemple: Transfer-Encoding: gzip, chunked
std::string Header_request::Transfer_Encoding_toString()
{
    if (this->Transfer_Encoding.empty())
        return ("");
    std::string str;
    for (size_t i = 0; i < this->Transfer_Encoding.size(); i++)
    {
        if (!this->Transfer_Encoding.at(i).empty())
        {
            str.append(this->Transfer_Encoding.at(i));
            if (i < this->Accept_Language.size() - 1)
                str.append(", ");
        }
    }
    return ("Transfer-Encoding: " + str + "\n");
}

// syntax: User-Agent: <product> / <product-version> <comment>
// exemple: User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36
std::string Header_request::User_Agent_toString()
{
    if (this->User_Agent.empty())
        return ("");

    return ("User-Agent: " + this->User_Agent + "\n");
}

// syntax: WWW-Authenticate: <type> realm=<realm>
// exemple: WWW-Authenticate: Basic
// exemple: WWW-Authenticate: Basic realm="Accès au site de staging", charset="UTF-8"
std::string Header_request::WWW_Authenticate_toString()
{
    if (this->WWW_Authenticate.type.empty())
        return ("");
    std::string str;
    str.append(this->WWW_Authenticate.type + " ");
    if (!this->WWW_Authenticate.realm.empty())
        str.append("realm=" + this->WWW_Authenticate.type + ", ");
    if (!this->WWW_Authenticate.charset.empty())
        str.append("charset=" + this->WWW_Authenticate.charset + " ");

    str.resize(str.size() - 1);

    return ("WWW-Authenticate: " + str + "\n");
}