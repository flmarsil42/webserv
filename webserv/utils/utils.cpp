/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fassani <fassani@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/02 17:41:23 by fassani           #+#    #+#             */
/*   Updated: 2021/06/08 18:17:13 by fassani          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utils.hpp"
#include <ctime>

std::vector<std::string> split_string(std::string str, std::string delimiter)
{
    std::vector<std::string> list;

    size_t position = 0;
    std::string token;
    while ((position = str.find(delimiter)) != std::string::npos)
    {
        token = str.substr(0, position - 1);
        list.push_back(token);
        str.erase(0, position + delimiter.size());
    }
    list.push_back(str);

    return (list);
}

std::string random_string(int size)
{
    srand(time(0));
    char rand_char[26] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
    std::string str = "";
    for (int i = 0; i < size; i++)
        str = str + rand_char[rand() % 26];
    return (str);
}

std::string generate_random_tmp_path()
{
    std::time_t result = std::time(NULL);
    std::string path = to_string(result) + "_" + random_string(10) + ".tmp";
    return (path);
}

std::string get_Path_Extention(std::string path)
{
    std::string::size_type position;
    position = path.rfind('.');

    if (position == std::string::npos)
        return ("");
    return (path.substr(position + 1));
}

//https://linux.die.net/man/2/stat
int fileExist(std::string path)
{
    struct stat fileStat;
    stat(path.c_str(), &fileStat);
    if (S_ISREG(fileStat.st_mode))
        return (1); //is regular file
    else if (S_ISDIR(fileStat.st_mode))
        return (2); //is directory
    else if (S_ISLNK(fileStat.st_mode))
        return (3); //is symbolic link
    return (0);
}

char *readBinaryFile(std::string path, std::streampos *size)
{
    char *memblock;

    std::ifstream file(path.c_str(), std::ios::in | std::ios::binary | std::ios::ate);
    if (file.is_open())
    {
        *size = file.tellg();
        memblock = new char[*size];
        file.seekg(0, std::ios::beg);
        file.read(memblock, *size);
        file.close();

        // std::cerr << "the entire file content is in memory" << std::endl;

        return (memblock);
    }
    else
        std::cerr << "Unable to open file" << std::endl;
    return 0;
}

void createtmpfile(std::string path)
{
    mkdir("./.tmp", 0777);

    std::ofstream file;
    file.open(path.c_str(), std::ofstream::out | std::ofstream::binary | std::ofstream::app);
    // if (file.is_open())
    // {
    //     for (size_t i = 0; i < rawRequest.size(); ++i)
    //         file << rawRequest[i];
    // }
    file.close();
}

std::string getPathFileName(std::string path)
{
    if (path.empty())
        return ("");

    std::string::size_type position;
    position = path.rfind('/');

    if (position == std::string::npos)
        return ("");
    return (path.substr(position + 1));
}

std::string getPathFileDir(std::string path)
{
    if (path.empty())
        return ("");

    std::string::size_type position;
    position = path.rfind('/');

    if (position == std::string::npos)
        return ("");
    return (path.substr(0, position));
}

std::string getCurrentDate(void)
{
    struct tm *date = NULL;
    char buff[64];
    struct timeval tv;

    gettimeofday(&tv, NULL);
    date = localtime(&(tv.tv_sec));
    strftime(buff, sizeof(buff), "%a, %d %b %Y %T GMT+02", date);
    return (std::string(buff));
}

std::string getPathLastModifiedDate(std::string path)
{
    if (path.empty())
    {
        return ("");
    }

    struct stat buf;
    struct tm *date = NULL;
    char time_buf[64];

    if (stat(path.c_str(), &buf) != -1)
    {
#if defined(__APPLE__)
        date = localtime(&buf.st_mtimespec.tv_sec);
#else
        date = localtime(&buf.st_mtim.tv_sec);
#endif
        strftime(time_buf, sizeof(time_buf), "%a, %d %b %Y %T GMT+02", date);
        return std::string(time_buf);
    }
    return ("");
}