#include "../Webserv.hpp"

/* Signal */
int HttpServer::signal = 0;

void HttpServer::setSignal(int value) {
	signal = value;
}

/* Constructor */
HttpServer::HttpServer() 
	: _hightSocket(0), _nbReadyFds(0) {}

/* Destructor */
HttpServer::~HttpServer() {}

/* Copy constructor */
HttpServer::HttpServer(const HttpServer& copy) {
	_fdSet[READ] = copy.getReadFds();
	_nbReadyFds = copy.getNbReadyFds();
	_ServerSocketHandler = copy.getServerSocketHandler();
	_ClientSocketHandler = copy.getClientSocketHandler();
}

/* Overloading operator */
const HttpServer& HttpServer::operator = (const HttpServer& assign) {
	_fdSet[READ] = assign.getReadFds();
	_nbReadyFds = assign.getNbReadyFds();
	_ServerSocketHandler = assign.getServerSocketHandler();
	_ClientSocketHandler = assign.getClientSocketHandler();
	return (*this);
}

/* Methods */
int HttpServer::parsing(std::string& filepath) {
	if (ConfigFileParser::checkConfigFile(_configFile, filepath) == FAILURE)
        return (FAILURE);
	if (ConfigFileParser::parseConfigFile(_configFile, _configServer) == FAILURE) {
		DEBUG_COUT("Erreur lors du parsing du fichier de configuration.");
		_configFile.close();
		return (FAILURE);
	}
	_configFile.close();
	DEBUG_COUT("Parsing du fichier de configuration serveur : OK.");
    return (SUCCESS);
}

// a supprimer une fois le parsing terminé
// std::list<ConfigServer> HttpServer::temporaryManualFillingConfigServer(int port) {
// 	std::list<ConfigServer> tmpList;
// 	std::list<std::string> tmpStringList;
// 	ConfigServer tmpConf;

// 	// Information bloc server
// 	// Placé quand meme dans un bloc location pour eviter des doublures d'attributs.
// 	// Chaque attributs dans Location peut se trouver aussi dans un bloc server sans bloc location
// 	tmpConf.setPort(port);
// 	tmpStringList.push_back("42WebServer");
// 	tmpConf.setServerNames(tmpStringList);
// 	tmpStringList.clear();

// 	// Recherche des codes status dans Syntax et compare avec les codes trouvé dans default.conf
// 	size_t i = 0;
// 	std::list<status_code_t> tmpStatusCodeList;
// 	int int_code_error = 404;
// 	while (Syntax::status_codes_tab[i].code_index != TOTAL_STATUS_CODE
// 		&& Syntax::status_codes_tab[i].code_int != int_code_error)
// 		i++;
// 	tmpStatusCodeList.push_back(Syntax::status_codes_tab[i].code_index);
	
// 	tmpConf.setErrorPageCodes(tmpStatusCodeList);
// 	tmpConf.setErrorPagePath("./error/");
// 	ConfigServer::Location tmpLocation;
// 	tmpLocation.setClientMaxBodySize(100);
// 	tmpLocation.setPath("/");
// 	tmpLocation.setRoot("/Users/admin/Desktop/webserv/www");
// 	tmpStringList.push_back("GET");
// 	tmpStringList.push_back("POST");
// 	tmpStringList.push_back("HEAD");
// 	tmpStringList.push_back("PUT");
// 	tmpLocation.setMethods(tmpStringList);
// 	tmpStringList.clear();
// 	tmpLocation.setAutoIndex(true);
// 	tmpStringList.push_back("index.html");
// 	//tmpStringList.push_back("index.php");
// 	tmpStringList.push_back("index2.html");
// 	tmpLocation.setIndex(tmpStringList);
// 	tmpStringList.clear();
// 	tmpLocation.setUploadStore("./");
// 	tmpLocation.setCGIpath("/usr/bin/php-cgi");
// 	tmpLocation.setCGIextention("*.php");

// 	tmpConf.addLocationBloc(tmpLocation);

// 	// Premier bloc location dans le default.conf
// 	ConfigServer::Location tmpLocation0;
// 	tmpLocation0.setPath("/test");
// 	tmpLocation0.setRoot("/Users/admin/Desktop/webserv/www");
// 	tmpStringList.push_back("index.html");
// 	tmpStringList.push_back("index");
// 	tmpLocation0.setIndex(tmpStringList);
// 	tmpStringList.clear();
// 	tmpLocation.setCGIpath("/usr/bin/php-cgi");
// 	tmpLocation.setCGIextention("*.php");

// 	tmpConf.addLocationBloc(tmpLocation0);

// 	// Deuxieme bloc location dans le default.conf
// 	ConfigServer::Location tmpLocation1;
// 	tmpLocation1.setPath("/put_test");
// 	tmpStringList.push_back("PUT");
// 	tmpLocation1.setMethods(tmpStringList);
// 	tmpStringList.clear();
// 	tmpLocation1.setRoot("./");

// 	tmpConf.addLocationBloc(tmpLocation1);
	
// 	// Troisieme bloc location dans le default.conf
// 	ConfigServer::Location tmpLocation2;
// 	tmpLocation2.setPath("/bmp");
// 	tmpLocation2.setAutoIndex(true);

// 	tmpConf.addLocationBloc(tmpLocation2);

// 	tmpList.push_back(tmpConf); 	// importation du bloc server dans list
// 	return (tmpList);
// }

// void HttpServer::_addNewServerSocket(ServerSocket& newServerSocket) {
// 	newServerSocket.createListeningSocket();
// 	_ServerSocketHandler.push_back(newServerSocket);
// }

// verification du remplissage des blocs Location
// a supprimer une fois terminé
// int HttpServer::parsingVerificationDebugger() {
// 	std::list<ConfigServer>::iterator it1 = _configServer.begin();
// 	std::list<ConfigServer>::iterator ite1 = _configServer.end();
// 	size_t x = 1;

// 	for (; it1 != ite1 ; ++it1) {
// 		std::cout << YELLOW << "*** Bloc server numero : " << x++ << RESET << std::endl;
// 		std::cout << GREEN << "hostname : " << it1->getHostname() << RESET << std::endl;
// 		std::cout << GREEN << "listen : " << it1->getPort() << RESET << std::endl;
// 		std::cout << GREEN << "server names : " << it1->getServerNames().empty() << RESET << std::endl;
// 		std::cout << GREEN << "error page path  : " << it1->getErrorPagePath() << RESET << std::endl;
// 		std::cout << GREEN << "error page codes : " << it1->getErrorPageCodes().empty() << RESET << std::endl;
// 		std::cout << std::endl; 

// 		std::list<ConfigServer::Location> listLoc = it1->getLocationBlocs();
// 		std::list<ConfigServer::Location>::iterator it2 = listLoc.begin();
// 		std::list<ConfigServer::Location>::iterator ite2 = listLoc.end();

// 		size_t i = 1;
// 		for (; it2 != ite2 ; ++it2) {
// 			if (it2 != --listLoc.end())
// 				std::cout << BLUE << "*** Bloc location numero : " << i++ << RESET << std::endl;
// 			else 
// 				std::cout << BLUE << "*** Bloc location du serveur par default : " << RESET << std::endl;
// 			std::cout << CYAN << "path : " << it2->getPath() << RESET << std::endl;
// 			std::cout << CYAN << "root : " << it2->getRoot() << RESET << std::endl;
// 			std::cout << CYAN << "methods : " << it2->getMethods().empty() << RESET << std::endl;
// 			std::cout << CYAN << "index : " << it2->getIndex().empty() << RESET << std::endl;
// 			std::cout << CYAN << "client max body size : " << it2->getClientMaxBodySize() << RESET << std::endl;
// 			std::cout << CYAN << "auto index : " << it2->getAutoIndex() << RESET << std::endl;
// 			std::cout << CYAN << "upload store : " << it2->getUploadStore() << RESET << std::endl;
// 			std::cout << CYAN << "cgi extensions : " << it2->getCGIextention() << RESET << std::endl;
// 			std::cout << CYAN << "cgi path : " << it2->getCGIpath() << RESET << std::endl;
// 			// std::cout << CYAN << "auth basic : " << it2->getAuthBasic() << RESET << std::endl;
// 			// std::cout << CYAN << "auth basic user file : " << it2->getAuthBasicUserFile() << RESET << std::endl;
// 			// verifier autre info
// 			std::cout << std::endl; 
// 		}
// 	}
// 	return (SUCCESS);
// }

int HttpServer::setupServers() {

	// _configServer = temporaryManualFillingConfigServer(8080);	// a supprimer une fois le parsing terminé
	// parsingVerificationDebugger();

	ServerSocket newServerSocket;

	std::list<ConfigServer>::iterator it = _configServer.begin();
	std::list<ConfigServer>::iterator ite = _configServer.end();
	for (; it != ite ; ++it) {
		newServerSocket.setConfigServ(&(*it)); 			// Importation du fichier de configuration dans un ServerSocket;
		newServerSocket.createListeningSocket();
		_ServerSocketHandler.push_back(newServerSocket);
	}
	return (SUCCESS);
}

void HttpServer::start() {
	_loopLauncher();
}

// Getters
fd_set HttpServer::getReadFds() const { return _fdSet[READ]; }
HttpServer::SOCKET HttpServer::getHightSocket() const { return _hightSocket; }
HttpServer::SOCKET HttpServer::getNbReadyFds() const { return _nbReadyFds; }
std::list<ServerSocket> HttpServer::getServerSocketHandler() const { return _ServerSocketHandler; }
std::list<ClientSocket> HttpServer::getClientSocketHandler() const { return _ClientSocketHandler; }
std::list<ConfigServer>& HttpServer::getConfigServer() { return _configServer; }
const std::list<ConfigServer>& HttpServer::getConfigServer() const { return _configServer; }


/* Private Methods */


// void HttpServer::_addNewClientSocket(SOCKET newClientSocket, PORT servPort, SOCK_ADDR newCliAddr) {
// 	_ClientSocketHandler.push_back(ClientSocket(newClientSocket, servPort, newCliAddr));
// }
void HttpServer::_addNewClientSocket(SOCKET newSocket, SOCK_ADDR newCliAddr, const std::list<const ConfigServer*>& configServer, int port) {
	_ClientSocketHandler.push_back(ClientSocket(newSocket, newCliAddr, configServer, port));
}

void HttpServer::_addServerSocketToFdset(std::list<ServerSocket>& serverSocket) {
    std::list<ServerSocket>::iterator it = serverSocket.begin();
    std::list<ServerSocket>::iterator ite = serverSocket.end();

    for (; it != ite ; ++it) {
        FD_SET(it->getSocket(), &_fdSet[READ]);
    }
}

void HttpServer::_addClientSocketToFdset(std::list<ClientSocket>& clientSocket) {
    std::list<ClientSocket>::iterator it = clientSocket.begin();
    std::list<ClientSocket>::iterator ite = clientSocket.end();

    for (; it != ite ; ++it) {
        FD_SET(it->getSocket(), &_fdSet[READ]);
        FD_SET(it->getSocket(), &_fdSet[WRITE]);
    }
}

void HttpServer::_writingHandler() {
	std::list<ClientSocket>::iterator it = _ClientSocketHandler.begin();
	std::list<ClientSocket>::iterator ite = _ClientSocketHandler.end();

	for (; it != ite ; ++it) {
		if (FD_ISSET(it->getSocket(), &_fdSet[WRITE]) && it->sendHttpResponse() == FAILURE) {
			close(it->getSocket());
			it = _ClientSocketHandler.erase(it);
			continue ;
		}
	}
}

void HttpServer::_readingHandler() {
	if (signal != 0)
		return ;

	std::list<ClientSocket>::iterator it = _ClientSocketHandler.begin();
	std::list<ClientSocket>::iterator ite = _ClientSocketHandler.end();

    for (; it != ite ; ++it) {
		SOCKET 	clientSocket = it->getSocket();
		char  	buffer[BUFFER_SIZE_REQUEST + 1];

		if (FD_ISSET(clientSocket, &_fdSet[READ])) {
			memset((char*)buffer, 0, BUFFER_SIZE_REQUEST + 1);
			if (it->getConnectionAuthorisation() == true) { // NON AUTHORIZED 
				it->processConnectionRefused();
				return ;
			}
			int lenRecptMsg;
			if ((lenRecptMsg = recv(clientSocket, (char*)buffer, BUFFER_SIZE_REQUEST, 0)) <= 0) {
				if (lenRecptMsg == 0) {
					DEBUG_COUT("Connexion avec le socket " << clientSocket << " terminée");
				}
				else {
					DEBUG_COUT("Problème durant la lecture du socket " << clientSocket);
				}
				close(it->getSocket());
				it = _ClientSocketHandler.erase(it);
				continue ;
			}
			it->receiveHttpRequest(buffer, lenRecptMsg);

			if (it->getHttpRequest().getStatusRequest() == HttpRequest::REQUEST_RECEIVED)
				it->process();
		}
    }
}

void HttpServer::_connexionHandler() {
    std::list<ServerSocket>::iterator it = _ServerSocketHandler.begin();
    std::list<ServerSocket>::iterator ite = _ServerSocketHandler.end();

    for (; it != ite ; ++it) {
		SOCKET 		newClientSocket;
		SOCK_ADDR 	cliAddr;
		socklen_t 	lenAddr = sizeof(cliAddr);
		memset((char*) &cliAddr, 0, (int) lenAddr);

    	if (FD_ISSET(it->getSocket(), &_fdSet[READ])) {
        	if ((newClientSocket = accept(it->getSocket(), (struct sockaddr*)& cliAddr, &lenAddr)) < 0 && signal == 0) {
				DEBUG_COUT("Problème lors de l'établissement de la connexion client - serveur ..." << strerror(errno));
				if (errno == EMFILE) { // EMFILE = to many open files
					DEBUG_COUT("Attaque DDOS detectée : le serveur va s'eteindre ...");
					signal = SIGINT;
				}
				return ;
			}
			if (fcntl(newClientSocket, F_SETFL, O_NONBLOCK) < 0) {
				DEBUG_COUT("Fcntl erreur avec F_SETFL : " << strerror(errno));
				exit(EXIT_FAILURE);
			}
			_addNewClientSocket(newClientSocket, cliAddr, ConfigServer::configServerListCreator(getConfigServer(), it->getPort()), it->getPort());
			DEBUG_COUT("Connexion entrante : " << inet_ntoa(cliAddr.sin_addr) << "...");
			DEBUG_COUT("Nouveau client ajouté sur le socket " << newClientSocket);
      	}
    }
}

void HttpServer::_selectHandler() {
    FD_ZERO(&_fdSet[READ]);
	FD_ZERO(&_fdSet[WRITE]);

    _addServerSocketToFdset(_ServerSocketHandler);
    _addClientSocketToFdset(_ClientSocketHandler);

    if ((_nbReadyFds = select(FD_SETSIZE, &_fdSet[READ], &_fdSet[WRITE], NULL, NULL)) < 0 && signal == 0) {
		std::cerr << RED << "Erreur select()." << std::endl;
		std::cerr << strerror(errno) << RESET << std::endl;
		_closeAllSocket(_ServerSocketHandler);
		_closeAllSocket(_ClientSocketHandler);
		exit(EXIT_FAILURE);
	}
}

void HttpServer::_loopLauncher() {
	if (_ServerSocketHandler.empty()) {
		DEBUG_COUT("Fichier de configuration vide");
		signal = SIGINT;
	}
    while (signal == 0) {
		_selectHandler();
		if (_nbReadyFds != 0 && signal == 0) {
			_connexionHandler();
			_readingHandler();
			_writingHandler();
		}
    }
	_closeAllSocket(_ServerSocketHandler);
	_closeAllSocket(_ClientSocketHandler);
}
