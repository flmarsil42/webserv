#ifndef CLIENTSOCKET_HPP
#define CLIENTSOCKET_HPP

#include "../Webserv.hpp"
#include "../server/HttpRequest.hpp"
#include "../server/HttpResponse.hpp"
#include "../server/HttpRequestParser.hpp"

/*
    ClientSocket permet d'enregistrer les informations
    d'un client suite à une demande de connexion au serveur.
    Son adresse IP, port d'écoute ... etc est enregistré dans sa
    SOCK_ADDR.
    Lors de l'acceptation de sa connexion avec accept(), un nouveau socket 
    lui est attribué automatiquement pour continuer la communication.
    ClientSocket est ensuite mis dans une liste sur HttpServer.
    Il recoit ensuite, la requêtes Http et la place dans l'objet HttpRequest.
*/

class ConfigServer;

class ClientSocket
{
public:
    /* Aliases definition  */
    typedef int                 SOCKET;
    typedef struct sockaddr_in  SOCK_ADDR;
    typedef int                 PORT;
    typedef uint8_t             byte;

public:
    /* Constructor */
    ClientSocket(SOCKET newSocket, SOCK_ADDR newCliAddr, const std::list<const ConfigServer*>& configServer, int port);
    /* Destructor */
    ~ClientSocket();
    /* Copy constructor */
    ClientSocket(const ClientSocket& copy);
    /* Overloading operator */
    const ClientSocket& operator = (const ClientSocket& assignObj);
    /* Methods */
    void receiveHttpRequest(const char* buffer, int lenRecptMsg);
    int sendHttpResponse();

    int process();
    int processConnectionRefused();

    // Getters
    PORT getPort() const;
    SOCKET getSocket() const;
    SOCK_ADDR getCliAddr() const;
    const HttpRequest& getHttpRequest() const;
    HttpRequest& getHttpRequest();
    HttpResponse& getServResponse();
    const char* getBuffer() const;
    bool getConnectionAuthorisation() const;
    bool getConnnectionStatus() const;
    const std::list<const ConfigServer*> &getConfigServ() const;
    
    // Setters
    void setClosing(bool);

private:
    /* Attributs */
    SOCKET          _newSocket;     // socket de transmission attribué par le serveur
    PORT            _servPort;      // port du serveur sur lequel la connexion est établie
    SOCK_ADDR       _cliAddr;       // informations client (IP, port ...)
    HttpRequest     _cliRequest;
    HttpResponse    _servResponse;
    bool            _connectionRefused;
    bool            _closing;
    const std::list<const ConfigServer*> _configServ;

};  // class ClientSocket

#endif
