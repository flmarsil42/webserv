/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cgi.hpp                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fassani <fassani@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/16 16:19:45 by fassani           #+#    #+#             */
/*   Updated: 2021/06/01 18:23:01 by fassani          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CGI_HPP
#define CGI_HPP

#include "../Webserv.hpp"
#include <iostream>

class Cgi
{
    public:
        static std::string execute(std::string cgi_path, ClientSocket &clientsocket);
};

#endif